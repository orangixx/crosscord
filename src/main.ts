import EventEmitter from 'events'

import { IClient } from './resources/IClient'

/**
 * Represents credentials for a service
 * @protected
 */
interface AuthSet {
	client: IClient
	token: string
}

/**
 * Represents a Crosscord meta-client
 * @extends {Crosscord}
 */
export class Client extends EventEmitter implements IClient {
	public client: IClient[] = []
	public service = 'Crosscord'

	/**
	 * Create a Crosscord meta-client
	 * @param auth An array of objects with `client`, a Crosscord class, and `token`, a token
	 *             for the corresponding service
	 */
	public constructor(private readonly auth: AuthSet[]) {
		super({
			captureRejections: true,
		})

		this.auth.forEach((set) => this.client.push(set.client))

		this.client.forEach((client) =>
			client.on('event', (event: string, ...args: any[]) =>
				this.emit(event, ...args),
			),
		)
	}

	/**
	 * Log into your service clients
	 */
	public login() {
		this.auth.forEach((set) =>
			/**
			 * This has to be async so it continues to the next forEach.
			 * However, forEach will wait for async to return, so this must be a nested function.
			 */
			(async () => {})().then(() => set.client.login(set.token)),
		)
	}
}

export { Base } from './resources/Base'
export { Channel } from './resources/Channel'
export { Guild } from './resources/Guild'
export { IClient } from './resources/IClient'
export { Message } from './resources/Message'
export { User } from './resources/User'
