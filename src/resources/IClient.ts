import EventEmitter from 'events'

/**
 * The main hub for interacting with a service API.
 * @extends {EventEmitter}
 */
export interface IClient extends EventEmitter {
	/** The backend library client */
	client: any

	/** The name of the service */
	service: string

	/** Log in to a service */
	login(token: string): void
}
