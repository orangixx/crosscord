import { Base } from './Base'
import { Channel } from './Channel'
import { User } from './User'

/**
 * Represents a message
 * @extends {Base}
 */
export interface Message extends Base {
	/** The author of the message */
	author: User

	/** The content of the message */
	content: string

	/** The channel that the message was sent in */
	channel: Channel

	/** Whether the message is deletable by the client user */
	readonly deletable: boolean

	/** Delete the message */
	delete(): void
}
