import { Base } from './Base'
import { Channel } from './Channel'

/**
 * Represents a guild or space
 * @extends {Base}
 */
export interface Guild extends Base {
	/** The name of the guild */
	name: string

	/** Array of channels in the guild */
	channels: Channel[]
}
