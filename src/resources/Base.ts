import { IClient } from './IClient'

/**
 * Represents a data model
 */
export interface Base {
	/** The client that instantiated this */
	readonly client: IClient

	/** The original object from the backend library */
	originator: any

	/** The service this comes from */
	service: string
}
