import { Base } from './Base'
import { Channel } from './Channel'

/**
 * Represents a user
 * @extends {Base}
 */
export interface User extends Base {
	/** The DM between the client's user and this user */
	dmChannel: Channel

	/** The full "tag" (e.g. `hydrabolt#0001`) for this user */
	tag: string

	/** The username of the user */
	username: string
}
