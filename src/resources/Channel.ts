import { PathLike } from 'fs'

import { Base } from './Base'
import { Guild } from './Guild'

interface MessageOptions {
	files: PathLike[]
}

/**
 * Any text-based channel/room
 * @extends {Base}
 */
export interface Channel extends Base {
	/** Whether the channel is deletable by the client user */
	readonly deletable: boolean

	/** The guild the channel is in (if in a {@link Guild}) */
	guild: Guild | null

	/** The name of the channel (if in a {@link Guild}) */
	name: string | null

	/**
	 * Sends a message in this channel.
	 * @param {content}
	 */
	send(content: string | PathLike, options?: MessageOptions): void
}
