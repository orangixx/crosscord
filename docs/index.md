## The basics

Crosscord is a simple event redirection script and some types.

You need to import and initialize Client and some service adapters:

```js
import { Client } from 'crosscord'
import { SomeService } from 'crosscord-service'

const client = new Client([
	{
		client: new SomeService(),
		token: myToken,
	},
])
```

Client is an EventEmitter, so simply use `Client.on` to add events:

```js
client.on('message', (message) => {
	if (!message.startsWith('!ping')) message.channel.send('pong')
})
```

Then log in. This uses the AuthSet[] provided in the constructor so you don't have to
give tokens again.

```js
client.login()
```
