rm -rf dist/
npx tsc
cp ./README.md ./package.json ./LICENSE ./dist/
cd dist/
npm publish
rm -rf dist/
