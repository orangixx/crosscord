# crosscord

typescript cross-messenger bot library

this is not a dropin replacement for matrix-js-sdk, discord.js, even with adapters.
non-cross-platform methods will not be implemented, and bots should instead use the
`originator` and `service` properties to implement them.
